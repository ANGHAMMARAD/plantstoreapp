import React from 'react';
import {
    Link,
    Redirect
} from "react-router-dom";
import './css/Dashboard.css'
import Menu from './Menu'
import Bottom from './Bottom'
import { setUserSession } from './login/storage_oper';

class Login extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            loading: false,
            phone: null,
            pwd: null,
            redirect: false
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    // handle button click of login form
    handleLogin() {
        if (this.state.phone != null && this.state.pwd != null) {
            this.setState({ error: null })
            this.setState({ loading: true })
            fetch(process.env.REACT_APP_api + '/users/signin', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    phone: this.state.phone,
                    password: this.state.pwd
                })
            })
                .then(res => res.json())
                .then(res => {
                    console.log(res)
                    if (res.error) throw res;
                    this.setState({ loading: false })
                    setUserSession(res.token, res.user);
                    this.setState({redirect: true})
                }).catch(error => {
                    this.setState({ loading: false })
                    this.setState({ error: error.message })
                });
        }
        else this.setState({ error: "Enter your data correctly", loading: false })
    }
    render() {
        if(this.state.redirect) return (<Redirect to="/dashboard"/>)
        return (
            <div className="root">
                <Menu />
                <div className="wrapper">
                    <div className="content-box content-box-center">
                        <div className="container register-form">
                            <div className="form">
                                <div className="note">
                                    <p>Sign in</p>
                                </div>
                                <div className="form-content login-form">
                                    <div className="row login-row">
                                        <div className="form-group">
                                            <input type="text" name="phone" onChange={(event) => this.setState({ phone: event.target.value })} className="form-control" placeholder="Phone Number *" />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" name="pwd" onChange={(event) => this.setState({ pwd: event.target.value })} className="form-control" placeholder="Your Password *" />
                                        </div>
                                    </div>
                                    {this.state.error && <><small style={{ color: 'red' }}>{this.state.error}</small><br /></>}<br />
                                    <button type="button" className="btnSubmit" onClick={this.handleLogin} disabled={this.state.loading}>
                                        {this.state.loading ? 'Loading...' : 'Login'}
                                    </button><br />
                                    <Link to="/registration">Go to Registration</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Bottom />
            </div>

        );
    }
}
export default Login;