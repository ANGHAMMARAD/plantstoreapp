import React from 'react'
import './css/Carousel.css'

export default function Carousel(props){
    return (
        <div id="carouselExampleControls" className="carousel slide" data-interval="false" data-ride="carousel">
                    <div className="carousel-inner">
                        {
                            props.photos.map((photo, id) =>
                                (id === 0) ? (
                                    <div key={id} className="carousel-item active">
                                        <img className="d-block w-100 carousel-img" src={photo} alt="First slide" />
                                    </div>
                                ) :
                                    (
                                        <div key={id} className="carousel-item">
                                            <img className="d-block w-100 carousel-img" src={photo} alt="First slide" />
                                        </div>
                                    )
                            )
                        }
                    </div>
                    <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>
                    <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>
    );
}