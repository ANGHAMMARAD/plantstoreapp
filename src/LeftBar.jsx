import React from 'react';
import './css/LeftBar.css'
import {
    Link,
} from "react-router-dom";

class LeftBar extends React.Component {
    render() {
        return (
            <nav id="sidebar">
                <div className="sidebar-header">
                    <h3>Категорії</h3>
                </div>
                <ul className="list-unstyled components">
                    <Link to={"/category/null"} key="-1">
                        <li >
                            <button type="button" className="btn btn-outline-primary leftBarElem">
                                Усі
                        </button>
                        </li>
                    </Link>
                    {this.props.categories.map(
                        category => (
                            <Link to={"/category/" + category.id} key={category.id}>
                                <li >
                                    <button type="button" className="btn btn-outline-primary leftBarElem">
                                        {category.type}
                                    </button>
                                </li>
                            </Link>
                        )
                    )}
                </ul>
            </nav>
        )
    }
}

export default LeftBar;