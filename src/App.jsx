import React from "react";
import {
  Switch,
  Route,
} from "react-router-dom";
import '../src/css/App.css'
import MainPage from '../src/pages/MainPage';
import CartPage from '../src/pages/CartPage';
import ProductPage from '../src/pages/ProductPage';
import CompositionPage from '../src/pages/CompositionPage';
import FavouritePage from '../src/pages/FavouritePage';
import CategoryPage from '../src/pages/CategoryPage';
import SearchPage from '../src/pages/SearchPage';
import { PrivateRoute, PublicRoute } from './login/SpecialRoutes'
import Login from './Login'
import Registration from './Registration'
// import AuthPage from '../src/pages/AuthPage'
import Dashboard from './Dashboard'
const api = process.env.REACT_APP_api;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: null,
      items: null,
      comps: null,
      lighting: null,
      watering: null,
      soil: null,
      sizes: null,
      wanted: [],
      cart: [],
      confirmed_sale: null,
      //Composition
      /*
      id_comp: 0,
    name: 'Comp name 1',
    rating: 4.3,
    photos: [
      'https://cnet1.cbsistatic.com/img/-B3kmqxu8sB6pYlTVZqRF9_cJB0=/2020/04/16/7d6d8ed2-e10c-4f91-b2dd-74fae951c6d8/bazaart-edit-app.jpg',
      'https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg',
      'https://media.gettyimages.com/photos/summer-poppies-at-sunset-picture-id108351473?s=612x612'
    ],
    plants:[
      0,1,3
    ]
      */

      // {row.id
      //   row.size_name}
      // name={row.name}
      // lname={row.lname}
      // price={row.price}
      // amount={row.amount}
      // image={row.photo}
    }
    this.addToWanted = this.addToWanted.bind(this);
    this.addToCart = this.addToCart.bind(this);
    this.updateCart = this.updateCart.bind(this);
    this.updateWanted = this.updateWanted.bind(this);
    this.getProduct = this.getProduct.bind(this);
    this.getComposition = this.getComposition.bind(this);
    this.confirmSale = this.confirmSale.bind(this)
  }

  componentDidMount() {
    fetch(api + "/api/type")
      .then(res => res.json())
      .then(
        res => {
          let copy = [];
          res.map(elem => copy.splice(elem.id, 0, elem));
          this.setState({
            categories: copy,
          });
        },
        err => {
          console.log("Loading categories failed:\n" + err)
        }
      )

    fetch(api + "/api/goods")
      .then(res => res.json())
      .then(
        res => {
          let copy = [];
          res.map(elem => copy.splice(elem.id, 0, elem));
          this.setState({
            items: copy,
          });
        },
        err => {
          console.log("Loading products failed:\n" + err)
        }
      )

    fetch(api + "/api/lighting")
      .then(res => res.json())
      .then(
        res => {
          let copy = [];
          res.map(elem => copy.splice(elem.id, 0, elem));
          this.setState({
            lighting: copy,
          });
        },
        err => {
          console.log("Loading lighting types failed:\n" + err)
        }
      )

    fetch(api + "/api/watering")
      .then(res => res.json())
      .then(
        res => {
          let copy = [];
          res.map(elem => copy.splice(elem.id, 0, elem));
          this.setState({
            watering: copy,
          });
        },
        err => {
          console.log("Loading watering types failed:\n" + err)
        }
      )

    fetch(api + "/api/soil")
      .then(res => res.json())
      .then(
        res => {
          let copy = [];
          res.map(elem => copy.splice(elem.id, 0, elem));
          this.setState({
            soil: copy,
          });
        },
        err => {
          console.log("Loading soil types failed:\n" + err)
        }
      )

    fetch(api + "/api/sizes")
      .then(res => res.json())
      .then(
        res => {
          this.setState({
            sizes: res,
          });
        },
        err => {
          console.log("Loading sizes failed:\n" + err)
        }
      )

    fetch(api + '/api/composition')
      .then(res => res.json())
      .then(
        res => {
          let comps = [];
          res.map(elem => comps.splice(elem.id_comp, 0, elem));
          this.setState({
            comps: comps,
          });
        },
        err => {
          console.log("Loading compositions failed:\n" + err)
        }
      )
  }

  confirmSale(cart) {
    this.setState({ confirmed_sale: cart })
  }

  async getProduct(id) {
    let result = this.state.items.find(elem => elem.id == id);
    console.log(result)
    if (result != null) return result;
    else {
      try {
        console.log("MAking fetch request - plant " + id)
        const res = await fetch(api + '/api/goods/' + id);
        if (!res.ok) throw Error(res.statusText);
        else {
          const parsed = await res.json();
          if (parsed != null)
            this.setState({ items: this.state.items.concat({ parsed }) })
          return parsed;
        }
      }
      catch (err) {
        console.log(err);
        return null;
      }
    }
  }

  async getComposition(id) {
    let result = this.state.comps.find(elem => elem.id == id);
    if (result != null) return result;
    else {
      try {
        console.log("MAking fetch request - composition " + id)
        const res = await fetch(api + '/api/composition/' + id);
        if (!res.ok) throw Error(res.statusText);
        else {
          const parsed = await res.json();
          if (parsed != null)
            this.setState({ comps: this.state.comps.concat({ parsed }) })
          return parsed;
        }
      }
      catch (err) {
        console.log(err);
        return null;
      }
    }
  }

  addToWanted(id) {
    this.setState({ wanted: this.state.wanted.concat(id) })
  }

  updateWanted(new_wanted) {
    this.setState({ wanted: new_wanted });
  }

  addToCart(product) {
    this.setState({ cart: this.state.cart.concat(product) });
  }

  updateCart(new_cart) {
    this.setState({ cart: new_cart });
  }

  render() {
    if (!this.state.items || !this.state.categories ||
      !this.state.lighting || !this.state.watering ||
      !this.state.soil || !this.state.sizes || !this.state.comps) return null;
    let server_data = {
      categories: this.state.categories,
      items: this.state.items,
      comps: this.state.comps,
      lighting: this.state.lighting,
      watering: this.state.watering,
      soil: this.state.soil,
      sizes: this.state.sizes,
      api: api,
      cart: this.state.cart,
      wanted: this.state.wanted,
    }

    return (
      <Switch>

        <PublicRoute path="/login" component={Login} />
        <PublicRoute path="/registration" component={Registration} />
        <Route path="/dashboard" render={props => (
          <Dashboard {...props}
            order={this.state.confirmed_sale}
            clear_sale={() => this.setState({ confirmed_sale: null })}
          />
        )} />
        <Route path="/search" render={(props) => (
          <SearchPage {...props}
            data={server_data}
            addToWanted={this.addToWanted}
            getProduct={this.getProduct}
          />
        )} />
        <Route path="/category/:id" render={(props) => (
          <CategoryPage {...props}
            data={server_data}
            addToWanted={this.addToWanted}
            getProduct={this.getProduct}

          />
        )} />
        <Route path="/product/:id" render={(props) => (
          <ProductPage {...props}
            data={server_data}
            addToWanted={this.addToWanted}
            addToCart={this.addToCart}
            getProduct={this.getProduct}
            getComposition={this.getComposition}
          />
        )} />
        <Route exact path="/cart" render={(props) => (
          <CartPage {...props}
            data={server_data}
            confirmSale={this.confirmSale}
            updateCart={this.updateCart}
          />)} />
        <Route path="/composition/:id" render={(props) => (
          <CompositionPage {...props}
            data={server_data}
            addToWanted={this.addToWanted}
            getProduct={this.getProduct}
          />)} />
        <Route path="/composition" render={(props) => (
          <CompositionPage {...props}
            data={server_data}
          // addToWanted={this.addToWanted}
          />)} />
        <Route path="/favourite" render={(props) => (
          <FavouritePage {...props}
            data={server_data}
            updateWanted={this.updateWanted}
          />)} />
        <Route path="/" render={(props) => (
          <MainPage {...props}
            data={server_data}
            addToWanted={this.addToWanted}
          />)} />
      </Switch>
    );
  }
}

export default App;
