import React from "react";
import './css/Cart.css'
import {
    Link
} from "react-router-dom";
import AddressWindow from './AddressWindow'

class Cart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cart: [],
            confirmation: false
        }
        this.totalSum = this.totalSum.bind(this);
        this.row_amount_change = this.row_amount_change.bind(this);
        this.deleteRow = this.deleteRow.bind(this);
        this.confirmSale = this.confirmSale.bind(this);
    }

    componentDidMount() {
        this.setState({ cart: this.props.cart })
        console.log("Cart mounted")
    }

    componentWillUnmount() {
        this.props.updateCart(this.state.cart);
        console.log("Cart unmounted")
    }

    confirmSale(client_info) {
        // console.log("here" + JSON.stringify(this.state.cart, null, 4))
        this.props.confirmSale({
            address: client_info,
            cart: this.state.cart.slice()
        });
        this.setState({ cart: [] })
    }

    totalSum() {
        let sum = 0;
        this.state.cart.slice().map((row) => { sum += row.amount * row.price })
        return sum;
    }

    row_amount_change(e, id, max) {
        if (e.currentTarget.value <= max && e.currentTarget.value >= 1) {
            let changed_cart = this.state.cart.slice().map((row, id_in_arr) => {
                if (id_in_arr !== id) return row;
                else {
                    let changed_row = row;
                    changed_row.amount = e.currentTarget.value;
                    return changed_row;
                }
            })
            this.setState({ cart: changed_cart });
        }
    }

    deleteRow(id_in_arr) {
        let copy = [...this.state.cart];
        copy.splice(id_in_arr, 1);
        this.setState({ cart: copy });
    }

    render() {
        if (this.totalSum() > 0) {
            if (this.state.confirmation) return (<AddressWindow confirm={this.confirmSale} />)
            return (
                <div className="container">
                    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
                    <table id="cart" className="table table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Товар</th>
                                <th style={{ width: '5%' }}>Розмір</th>
                                <th style={{ width: '10%' }}>Ціна</th>
                                <th style={{ width: '8%' }}>Кількість</th>
                                <th style={{ width: '10%' }} className="text-center">Сума рядку</th>
                                <th style={{ width: '10%' }} />
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.cart.map((row, id_in_arr) => (
                                    <Row key={row.id + '_' + row.size_id}
                                        id={row.id}
                                        id_in_arr={id_in_arr}
                                        name={row.name}
                                        lname={row.lname}
                                        price={row.price}
                                        amount={row.amount}
                                        max_amount={row.max_amount}
                                        image={row.photo}
                                        size_name={row.size_name}
                                        onChange={this.row_amount_change}
                                        deleteRow={this.deleteRow}
                                    />
                                ))
                            }
                        </tbody>
                        <tfoot>
                            <tr>
                                {/* <td><a href="#" className="btn btn-warning"><i className="fa fa-angle-left" /> Continue Shopping</a></td> */}
                                <td colSpan={4} className="hidden-xs" />
                                <td className="hidden-xs text-center"><strong>
                                    Усього: {this.totalSum()}грн
                    </strong></td>
                                <td><button className="btn btn-success btn-block" onClick={() => this.setState({ confirmation: true })}>Підтвердити</button></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            )
        }
        else return (
            <h2>Корзина порожня</h2>
        )
    }
}

function Row(props) {
    let title;
    if (props.lname) title = (
        <div className="product-info">
            <h5 className="nomargin">{props.name}</h5>
            <h6 className="nomargin">{props.lname}</h6>
        </div>
    );
    else title = (
        <div className="product-info">
            <h5 className="nomargin">{props.name}</h5>
        </div>
    );
    return (
        <tr>
            <td data-th="Товар">
                <Link to={'/product/' + props.id}>
                    <div className="row product-info-row">
                        <div className="cart-image-holder"><img src={props.image} alt="..." className="img-responsive" /></div>
                        {title}
                    </div>
                </Link>
            </td>
            <td data-th="Розмір">{props.size_name}</td>
            <td data-th="Ціна">{props.price}</td>
            <td data-th="Кількість">
                <input type="number" min={1} max={props.max_amount} onChange={(e) => { props.onChange(e, props.id_in_arr, props.max_amount) }} className="form-control text-center" defaultValue={1} />
                <span>Max: {props.max_amount}</span>
            </td>
            <td data-th="Сума рядку" className="text-center subtotal">{props.price * props.amount}</td>
            <td className="actions">
                <button className="btn btn-danger btn-sm" onClick={() => { props.deleteRow(props.id_in_arr) }}><i className="fa fa-trash-o" /></button>
            </td>
        </tr>
    );
}

export default Cart;