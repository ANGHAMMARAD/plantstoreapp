import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getToken } from './storage_oper';
 

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => getToken() ? <Component {...props} /> : <Redirect to='/login' />}
    />
  )
}

function PublicRoute({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={(props) => !getToken() ? <Component {...props} /> : <Redirect to='/dashboard' />}
      />
    )
  }

  export {
      PrivateRoute,
      PublicRoute
  }