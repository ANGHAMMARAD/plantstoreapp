import React from 'react';
import {
    Link,
} from "react-router-dom";
import './css/ProductSmall.css'

class CompositionSmall extends React.Component {

    render() {
        return (
            <div className="card s_p_container"  >
                <Link to={'/composition/' + this.props.data.id_comp}>
                    <img className="card-img-top s_p_image" src={this.props.data.photos[0]} alt="Composition card" />
                </Link>
                <div className="card-body">
                    <h5 className="card-title">{this.props.data.name}</h5>

                    <p className="card-rating">Рейтинг: {this.props.data.rating}</p>
                </div>
            </div>
        )
    }
}

export default CompositionSmall;