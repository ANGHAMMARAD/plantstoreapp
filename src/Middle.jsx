import React from 'react';
import ProductSmall from '../src/ProductSmall'

class Middle extends React.Component {
    render() {
        return (
            <div className="container items-container">
              {
                this.props.items.map((elem)=>(
                    <ProductSmall key={elem.id}
                    data = {elem}
                    wanted={this.props.wanted}
                    addToWanted={this.props.addToWanted}
                    />
                ))
              }
            </div>
        )
    }
}

export default Middle;