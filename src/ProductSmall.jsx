import React from 'react';
import {
    Link,
  } from "react-router-dom";
import './css/ProductSmall.css'

class ProductSmall extends React.Component {
    constructor(props) {
        super(props);
        this.addToWanted=this.addToWanted.bind(this);
    }

    addToWanted(e) {
        if ((this.props.wanted.indexOf(this.props.data.id)===-1)) {
            this.props.addToWanted(this.props.data.id);
            // this.setState({ used: "wanted" });
        }
    }

    render() {
        return (
            <div className="card s_p_container"  >
                <Link to={"/product/"+this.props.data.id}>
                <img className="card-img-top s_p_image" src={ this.props.data.photos[0] } alt="Card image cap" />
                </Link>
                <div className="card-body">
                    <h5 className="card-title">{this.props.data.name}</h5>
                    {(this.props.data.lname) ? (<h6 className="card-subtitle mb-2 text-muted">{
                        this.props.data.lname
                    }</h6>) : (<h6 className="card-subtitle mb-2 text-muted subtitle-placeholder"></h6>)}

                    <p className="card-rating">Рейтинг: {this.props.data.grade}</p>
                    <div className="s_p_pricerow">
                        <div className="s_p_price">
                            <span>{this.min_price(this.props.data.sizes)}</span>
                            <span>-</span>
                            <span>{this.max_price(this.props.data.sizes)}</span>
                            <span> грн</span>
                        </div>
                        {(this.props.wanted.indexOf(this.props.data.id)===-1) ? (
                        <button className="btn btn-primary " onClick={this.addToWanted}>У бажане</button>
                        ) : (
                            <button className="btn btn-primary disabled" disabled>Додано</button>
                        ) }
                    </div>
                </div>
            </div>
        )
    }

    min_price(sizes) {
        if (!sizes) return null;
        let min_size = null;
        for (let i = 0; i < sizes.length; ++i) {
            if (sizes[i].amount > 0) {
                min_size = sizes[i];
                break;
            }
        }
        sizes.forEach(size => {
            if (size.amount > 0 && size.price < min_size.price) {
                min_size = size;
            }
        });
        return (min_size) ? min_size.price : null;
    }
    max_price(sizes) {
        if (!sizes) return null;
        let max_size = null;
        for (let i = 0; i < sizes.length; ++i) {
            if (sizes[i].amount > 0) {
                max_size = sizes[i];
                break;
            }
        }
        sizes.forEach(size => {
            if (size.amount > 0 && size.price > max_size.price) {
                max_size = size;
            }
        });
        return (max_size) ? max_size.price : null;
    }
}

export default ProductSmall;