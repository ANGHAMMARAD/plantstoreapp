import React from 'react';
import { removeUserSession, getUser, getToken } from './login/storage_oper';
import {
  Link,
  Redirect
} from "react-router-dom";
import './css/Dashboard.css'
import Menu from './Menu'
import Bottom from './Bottom'

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logout: false
    }
    this.handleLogout=this.handleLogout.bind(this);
  }

  componentDidMount() {
    console.log("Dash mounted "+getToken());
    if (this.props.order!=null && getToken()) {
      console.log("Dash fetch");
      fetch(process.env.REACT_APP_api + "/orders", {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + getToken(),
        },
        body: JSON.stringify(this.props.order)
      }).then(res => res.json()
        .then(res => {
          console.log(res);
          this.props.clear_sale();
        }), err => {
          console.log(err);
        })
    }
  }

  handleLogout() {
    removeUserSession();
    this.setState({ logout: true })
  }

  render() {
    if (this.state.logout) return (<Redirect to="/" />)
    if (!getUser()) return (<Redirect to="/login" />)
    return (
      <div className="root">
        <Menu />
        <div className="wrapper">
          <div className="content-box content-box-center">
            <div className="dash-content">
              <div className="top">
                <h6>Your orders, User {getUser().phone}</h6>
                <input type="button" className="btn btn-outline-success btn-rounded btn-sm" onClick={this.handleLogout} value="Logout" />
              </div>

            </div>
          </div>
        </div>
        <Bottom />
      </div>

    );
  }

}

export default Dashboard;