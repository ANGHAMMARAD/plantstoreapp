import React from 'react';
import {
  Link,
} from "react-router-dom";
import './css/Menu.css'

class Menu extends React.Component {
    render() {
        return (
            <ul className="container-fluid menu-list">
              <li>
                <Link to="/">
                  <svg className="menu-icon" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 463.89 438.88" version="1.0">
                    <g id="layer1" transform="translate(-42.339 -276.34)">
                      <path id="rect2391" d="m437.15 499.44zl-162.82-144.19-162.9 144.25v206.12c0 5.33 4.3 9.6 9.62 9.6h101.81v-90.38c0-5.32 4.27-9.62 9.6-9.62h83.65c5.33 0 9.6 4.3 9.6 9.62v90.38h101.84c5.32 0 9.6-4.27 9.6-9.6v-206.18zm-325.72 0.06z"/>
                      <path id="path2399" d="m273.39 276.34l-231.05 204.59 24.338 27.45 207.65-183.88 207.61 183.88 24.29-27.45-231-204.59-0.9 1.04-0.94-1.04z"/>
                      <path id="rect2404" d="m111.43 305.79h58.57l-0.51 34.69-58.06 52.45v-87.14z"/>
                    </g>
                  </svg>
                </Link>
              </li>
                <li>
                  <Link to="/composition">
                    <button className="btn btn-outline-success btn-rounded btn-sm my-0">
                        Композиції
                    </button>
                  </Link>
                </li>
                <li>
                  <Link to="/search">
                    <button className="btn btn-outline-success btn-rounded btn-sm my-0">
                        Пошук
                    </button>
                  </Link>
                </li>
                <li>
                <Link to="/login">
                    <svg className="bi bi-person menu-icon" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M13 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM3.022 13h9.956a.274.274 0 0 0 .014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 0 0 .022.004zm9.974.056v-.002.002zM8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0z" />
                    </svg>
                </Link>
                </li>
                <li>
                <Link to="/favourite">
                    <svg className="bi bi-heart-fill menu-icon" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z" />
                    </svg>
                </Link>
                </li>
                <li>
                <Link to="/cart">
                    <svg className="bi bi-cart menu-icon" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fillRule="evenodd" d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm7 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
                    </svg>
                </Link>
                </li>
            </ul>
        );
    }
}



export default Menu;