import React from "react";

class Bottom extends React.Component{
  
  render(){
    return (
      <div className="bottom">
        <div>Icons made by <a href="https://www.flaticon.com/authors/itim2101" title="itim2101">itim2101</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a></div>
      </div>
    );
  }
}

export default Bottom;