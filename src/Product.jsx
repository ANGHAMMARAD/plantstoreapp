import React from 'react';
import './css/Product.css'
import Carousel from './Carousel'
import {
    Link
} from "react-router-dom";

class Product extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wanted_btn_used: null,
            used_sizes: [],
            choosen_size: null,
            comps: null,
        }
        this.addToWanted = this.addToWanted.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    componentDidMount() {
        let comps = [];
        Promise.all(
            this.props.data.compositions.map((elem) =>
                this.props.getComposition(elem)
            )
        ).then((values) => {
            values.forEach((elem) => {
                if(elem!=null) comps.splice(elem.id_comp, 0, elem)
            })
            this.setState({ comps: comps })
        });

        let used_sizes = [];
        this.props.data.sizes.map(size => {
            if (this.props.cart.find((row) => row.id === this.props.data.id && row.size_id === size.id)) {
                used_sizes.push(size.id);
            }
        })
        this.setState({
            wanted_btn_used: (this.props.wanted.indexOf(this.props.data.id) >= 0) ?
                "wanted" : null,
            used_sizes: used_sizes,
        })
    }

    addToWanted() {
        if (!this.state.wanted_btn_used) {
            this.props.addToWanted(this.props.data.id);
            this.setState({ wanted_btn_used: "wanted" });
        }
    }

    addToCart() {
        if (this.state.used_sizes.indexOf(this.state.choosen_size) === -1) {
            this.setState({ used_sizes: this.state.used_sizes.concat(this.state.choosen_size) });
            let product = {
                id: this.props.data.id,
                name: this.props.data.name,
                lname: this.props.data.lname,
                amount: 1,
                max_amount: this.props.data.sizes.find(elem=>elem.id == this.state.choosen_size).amount,
                size_id: this.state.choosen_size,
                size_name: this.props.sizes.find(elem=>elem.id == this.state.choosen_size).name,
                price: this.props.data.sizes.find((size) => this.state.choosen_size === size.id).price,
                photo: this.props.data.photos[0],
            }
            this.props.addToCart(product);
        }
    }

    watering_div() {
        return (<div className="watering">{
            this.props.watering.map((elem) => {
                if (elem.id === this.props.data.waterId) return (
                    <p key={elem.id} className="watering-active enviroment-element">{elem.description} потреба у поливі</p>
                )
                else return (
                    <p key={elem.id} className="enviroment-element">{elem.description} потреба у поливі</p>
                )
            })}
        </div>);
    }

    lighting_div() {
        return (<div className="lighting">{
            this.props.lighting.map((elem) => {
                if (this.props.data.light.indexOf(elem.id) >= 0) return (
                    <p key={elem.id} className="lighting-active enviroment-element">Освітлення: {elem.description}</p>
                )
                else return (
                    <p key={elem.id} className="enviroment-element">Освітлення: {elem.description}</p>
                )
            })}
        </div>);
    }

    soil_div() {
        return (<div className="soil">{
            this.props.soil.map((elem) => {
                if (this.props.data.light.indexOf(elem.id) >= 0) return (
                    <p key={elem.id} className="soil-active enviroment-element">Ґрунти: {elem.description}</p>
                )
                else return (
                    <p key={elem.id} className="enviroment-element">Ґрунти: {elem.description}</p>
                )
            })}
        </div>);
    }

    amount_left() {
        if (this.state.choosen_size != null) return (
            <p>Залишилось: {this.props.data.sizes.find(elem=>elem.id == this.state.choosen_size).amount} шт.</p>
        );
        return null;
    }

    choose_size() {
        return (
            this.props.data.sizes.map((size) => {
                if (size.amount > 0 && this.state.used_sizes.indexOf(size.id) === -1) return (
                    <div key={size.id} className="form-check-inline">
                        <label className="form-check-label">
                            <input type="radio" className="form-check-input" name="size" value={size.id}
                                onClick={() => this.setState({ choosen_size: size.id })} />
                            {this.props.sizes.find(elem => size.id==elem.id).name}
                        </label>
                    </div>
                )
                else return (
                    <div key={size.id} className="form-check-inline disabled">
                        <label className="form-check-label">
                            <input type="radio" className="form-check-input" name="size" value={size.id} disabled />
                            {this.props.sizes.find(elem => size.id==elem.id).name}
                        </label>
                    </div>
                )
            })
        )
    }

    price_and_buy() {
        if (this.state.choosen_size === null) {
            return (<div>
                <button className="btn btn-primary disabled" disabled>Оберіть розмір</button>
            </div>
            );
        }
        else {
            if (this.state.used_sizes.indexOf(this.state.choosen_size) === -1) {
                return (
                    <div>
                        <div className="product-price">Ціна: {this.props.data.sizes.find((size) => this.state.choosen_size === size.id).price}грн</div>
                        <button className="btn btn-primary" onClick={this.addToCart}>Купити</button>
                    </div>
                )
            }
            else {
                return (
                    <div>
                        <div className="product-price">Ціна: {this.props.data.sizes.find((size) => this.state.choosen_size === size.id).price}грн</div>
                        <button className="btn btn-primary disabled" disabled>У корзині</button>
                    </div>
                )
            }
        }
    }

    linkedCompositions(){
        if(this.state.comps.length==0) return null;
        return (
            <div className="bottom_block">
                    <h3>Рослина у складі композицій</h3>
                    <ul className="linked_compositions plants_of_composition">
                        {this.state.comps.map((comp) =>
                            <CompositionRow
                                key={comp.id_comp}
                                id={comp.id_comp}
                                name={comp.name}
                                photo={comp.photos[0]}
                            />)
                        }
                    </ul>
                </div>
        );
    }

    render() {
        if (this.state.comps == null) return null;
        return (
            <div className="product-grid-container">
                <div className="left">
                    <Carousel
                        photos={this.props.data.photos}
                    />
                    <div className="product-info">
                        <h2>{this.props.data.name}</h2>
                        <h3 className="text-muted">{this.props.data.lname}</h3>
                        <h4 className="rating">Рейтинг: {this.props.data.grade}</h4>
                        <p>{this.props.data.description}</p>
                        <p>Середня висота: {this.props.data.height}м</p>
                    </div>
                </div>
                <div className="right">
                    <div className="enviroment">
                        {this.watering_div()}
                        {this.lighting_div()}
                        {this.soil_div()}
                    </div>
                    <div className="commerce">
                        {this.amount_left()}
                        {(!this.state.wanted_btn_used) ? (
                            <button className="btn btn-primary " onClick={this.addToWanted}>У бажане</button>
                        ) : (this.state.wanted_btn_used === "wanted") ? (
                            <button className="btn btn-primary" disabled>Додано у бажане</button>
                        ) : (
                                    <button className="btn btn-primary" disabled>У корзині</button>
                                )}
                        <div className="choose_size">
                            <p>Виберіть розмір горщику:</p>
                            {this.choose_size()}
                        </div>
                        {this.price_and_buy()}
                    </div>
                </div>
                {this.linkedCompositions()}

            </div>
        )
    }

}

export default Product;

function CompositionRow(props) {
    return (
        <li>
            <div className="wanted_row">
                <Link to={'/composition/' + props.id}>
                    <div className="link_container">
                        <img src={props.photo} />
                        <div className="title_fav">
                        <h6>{props.name}</h6>
                        </div>
                    </div>
                </Link>
            </div>
        </li>
    )
}