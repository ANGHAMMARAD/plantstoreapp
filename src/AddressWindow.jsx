import { getUser } from './login/storage_oper';
import React from 'react';
import {
    Link,
    Redirect
} from "react-router-dom";
import './css/Dashboard.css'

class AddressWindow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            redirect: !getUser(),
            confirmed: false,
            country: null,
            region: null,
            city: null,
            district: null,
            street: null,
            house: null,
            flat: null,
            sale: null
        }
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    handleConfirm() {
        if (this.state.country == null || this.state.city == null || this.state.street == null || this.state.house == null) {
            this.setState({ error: "Please, write down all required data" })
        }
        else {
            this.setState({
                confirmed: true,
                sale: {
                    country: this.state.country,
                    region: this.state.region,
                    city: this.state.city,
                    district: this.state.district,
                    street: this.state.street,
                    house: this.state.house,
                    flat: this.state.flat
                },
            })
        }

    }

    componentWillUnmount() {
        if (this.state.confirmed) this.props.confirm(this.state.sale);
    }

    render() {
        if (this.state.redirect && this.state.confirmed) return (<Redirect to="/login" />)
        if (this.state.confirmed) return (<Redirect to="/dashboard" />)
        return (
            <div className="form-content login-form">
                <div className="row login-row">
                    <div className="form-group">
                        <input type="text" onChange={(event) => this.setState({ country: event.target.value })} className="form-control" placeholder="Country *" />
                    </div>
                    <div className="form-group passive">
                        <input type="text" onChange={(event) => this.setState({ region: event.target.value })} className="form-control" placeholder="Region" />
                    </div>

                </div>
                <div className="row login-row">
                    <div className="form-group">
                        <input type="text" onChange={(event) => this.setState({ city: event.target.value })} className="form-control" placeholder="City *" />
                    </div>
                    <div className="form-group passive">
                        <input type="text" onChange={(event) => this.setState({ district: event.target.value })} className="form-control" placeholder="District" />
                    </div>
                </div>
                <div className="row login-row">
                    <div className="form-group">
                        <input type="text" onChange={(event) => this.setState({ street: event.target.value })} className="form-control" placeholder="Street *" />
                    </div>
                    <div className="form-group passive">
                        <input type="text" onChange={(event) => this.setState({ flat: event.target.value })} className="form-control" placeholder="Flat number" />
                    </div>

                </div>
                <div className="row login-row">
                    <div className="form-group">
                        <input type="text" onChange={(event) => this.setState({ house: event.target.value })} className="form-control" placeholder="House Number *" />
                    </div>
                </div>
                {this.state.error && <><small style={{ color: 'red' }}>{this.state.error}</small><br /></>}<br />
                <button type="button" className="btnSubmit" onClick={this.handleConfirm} >
                    {this.state.redirect ? 'Sign in and Buy' : 'Buy'}
                </button><br />
            </div>
        )
    }
}

export default AddressWindow;