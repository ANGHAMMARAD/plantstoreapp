exports.min_price = (sizes) => {
    if (!sizes) return null;
    let min_size = null;
    for (let i = 0; i < sizes.length; ++i) {
        if (sizes[i].amount > 0) {
            min_size = sizes[i];
            break;
        }
    }
    sizes.forEach(size => {
        if (size.amount > 0 && size.price < min_size.price) {
            min_size = size;
        }
    });
    return (min_size) ? min_size.price : null;
}

exports.max_price = (sizes) => {
    if (!sizes) return null;
    let max_size = null;
    for (let i = 0; i < sizes.length; ++i) {
        if (sizes[i].amount > 0) {
            max_size = sizes[i];
            break;
        }
    }
    sizes.forEach(size => {
        if (size.amount > 0 && size.price > max_size.price) {
            max_size = size;
        }
    });
    return (max_size) ? max_size.price : null;
}