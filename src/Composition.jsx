import React from 'react';
import {
    Link,
  } from "react-router-dom";
import './css/Composition.css'
import Carousel from './Carousel'
const common = require('../src/common')

class Composition extends React.Component {
    constructor(props){
        super(props);
        this.state={
            plants: null,
        }
    }
    
    componentDidMount(){
        let plants = [];
        Promise.all(
            this.props.data.plants.map((elem) => 
                this.props.getProduct(elem)
            )
        ).then((values) => {
            values.forEach((elem)=>{
                if(elem!=null) plants.splice(elem.id, 0, elem)
            })
            this.setState({plants: plants})
        });        
    }

    render() {
        if(this.state.plants==null) return null;
        return (
        <div className="composition_container">
            <Carousel photos={this.props.data.photos}/>
            <h3>{this.props.data.name}</h3>
            <h3 className="rating">Рейтинг: {this.props.data.rating}</h3>
            <h3>Рослини у складі композиції</h3>
            <ul className="plants_of_composition">
            {
                this.state.plants.map(plant => (
                <Row 
                    data={plant}
                    key={plant.id}
                />
                ))
            }
            </ul>
            
        </div>
        )
    }
}

export default Composition;

function Row(props) {
    return (
        <li>
            <div className="wanted_row">
                <Link to={'/product/' + props.data.id}>
                    <div className="link_container">
                    <img src={props.data.photos[0]} />
                    <div className="fav_title">
                        <h5>{props.data.name}</h5>
                        <h6 className="text-muted">{props.data.lname}</h6>
                    </div>
                    <div className="fav_price">
                        <span>{common.min_price(props.data.sizes)}</span>
                        <span>-</span>
                        <span>{common.max_price(props.data.sizes)}</span>
                        <span> грн</span>
                    </div>
                    </div>
                </Link>
            </div>
        </li>
    )
}