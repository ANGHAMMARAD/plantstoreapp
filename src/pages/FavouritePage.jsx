import React from "react";
import {
//   Switch,
//   Route,
  Link
} from "react-router-dom";
import '../css/App.css'
import '../css/FavouritePage.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Bottom from '../Bottom'
const common = require('../common')

class FavouritePage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wanted: this.props.data.wanted,
        }
        this.deleteRow = this.deleteRow.bind(this);
    }

    componentWillUnmount() {
        this.props.updateWanted(this.state.wanted);
    }

    deleteRow(id_in_arr) {
        let copy = [...this.state.wanted];
        copy.splice(id_in_arr, 1);
        this.setState({ wanted: copy });
    }

    render() {
        return (
            <div className="root">
                <Menu api={this.props.data.api}
                />
                <div className="wrapper">
                    <LeftBar
                        categories={this.props.data.categories}
                        api={this.props.data.api}
                    />
                    <div className="content-box">
                        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
                        <div className="wanted_header">
                            <h2>Бажані товари</h2>
                        </div>
                        {(this.state.wanted.length === 0) ? (
                            <h4>Список порожній</h4>
                        ) : (
                                <ul className="wanted_list">
                                    {this.state.wanted.map((elem_id, id_in_arr) => {
                                        return (<WantedRow
                                            key={elem_id}
                                            id={elem_id}
                                            data={this.props.data.items[elem_id]}
                                            id_in_arr={id_in_arr}
                                            deleteRow={this.deleteRow}
                                        />)
                                    })}
                                </ul>
                            )}
                    </div>
                </div>
                <Bottom />
            </div>
        )
    }
}

export default FavouritePage;

function WantedRow(props) {
    return (
        <li>
            <div className="wanted_row">
                <Link to={'/product/' + props.data.id}>
                    <div className="link_container">
                    <img src={props.data.photos[0]} />
                    <div className="fav_title">
                        <p>{props.data.name}</p>
                        <p>{props.data.lname}</p>
                    </div>
                    <div className="fav_price">
                        <span>{common.min_price(props.data.sizes)}</span>
                        <span>-</span>
                        <span>{common.max_price(props.data.sizes)}</span>
                        <span> грн</span>
                    </div>
                    </div>
                </Link>
                <button className="btn btn-danger btn-sm" onClick={() => { props.deleteRow(props.id_in_arr) }}><i className="fa fa-trash-o" /></button>
            </div>
        </li>
    )
}