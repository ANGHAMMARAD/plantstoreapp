
import React from "react";
// import {
// } from "react-router-dom";
import '../css/App.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Middle from '../Middle'
import Bottom from '../Bottom'

class CategoryPage extends React.Component {
  constructor(props){
    super(props);
    this.state={
      current_items: null,
      id: -1,
    }

  }

  componentDidMount(){
    this.setState({id: this.props.match.params.id})
    this.setCategory(this.props.match.params.id)
  }

  componentDidUpdate(){
    if(this.props.match.params.id != this.state.id){
      this.setState({current_items: null})
      this.setState({id: this.props.match.params.id})
      this.setCategory(this.props.match.params.id)  
    }
  }

  async setCategory(id){
    if(id==null || this.props.data.categories.find(elem => elem.id==id)==null)    
    return this.setState({current_items: this.props.data.items.slice()});
    Promise.all(this.props.data.categories.find(elem => elem.id==id).plants.map(
      (plant_id)=>this.props.getProduct(plant_id)
      )).then((values)=>this.setState({current_items: values})
    , err => console.log(JSON.stringify(err, null, 4)))
  }

  render() {
    if(this.state.current_items==null)      return null;
    let found_type = this.props.data.categories.find((elem) => this.state.id == elem.id);
    let header = ((found_type!=null) ? found_type.type : null);
    return (
      <div className="root">
        <Menu api={this.props.data.api}
        />
        <div className="wrapper">
          <LeftBar
            categories={this.props.data.categories}
            api={this.props.data.api}
          />
          <div className="content-box">
            <h1>{header}</h1>
          <Middle 
            items={this.state.current_items}
            wanted={this.props.data.wanted}
            addToWanted={this.props.addToWanted}
          />
          </div>
        </div>
        <Bottom />
      </div>
    )
  }
}

export default CategoryPage;