import React from "react";
// import {
// } from "react-router-dom";
import '../css/App.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Middle from '../Middle'
import Bottom from '../Bottom'

class MainPage extends React.Component{

    render(){
        return (
            <div className="root">
        <Menu api={this.props.data.api}
        />
        <div className="wrapper">
          <LeftBar
            categories={this.props.data.categories}
            api={this.props.data.api}
          />
          <div className="content-box">
          <Middle 
            items={this.props.data.items}
            api={this.props.data.api} 
            wanted={this.props.data.wanted}
            addToWanted={this.props.addToWanted}
          />
          </div>
        </div>
        <Bottom />
      </div>
        )
    }
}

export default MainPage;