import React from "react";
// import {
// } from "react-router-dom";
import '../css/App.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Bottom from '../Bottom'
import Cart from '../Cart'

class CartPage extends React.Component {

  render() {
    return (
      <div className="root">
        <Menu api={this.props.data.api}
        />
        <div className="wrapper">
          <LeftBar
            categories={this.props.data.categories}
            api={this.props.data.api}
          />
          <div className="content-box">
          <Cart
            cart = {this.props.data.cart}
            confirmSale={this.props.confirmSale}
            updateCart={this.props.updateCart}
          />
         </div>
        </div>
        <Bottom />
      </div>
    )
  }
}

export default CartPage;