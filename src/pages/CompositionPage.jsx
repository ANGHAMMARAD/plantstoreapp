import React from "react";
// import {
//   // Switch,
//   // Route,
//   Link
// } from "react-router-dom";
import '../css/App.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Bottom from '../Bottom'
import Composition from '../Composition'
import CompositionSmall from '../CompositionSmall'

class CompositionPage extends React.Component {

  render() {
    return (
      <div className="root">
        <Menu api={this.props.data.api}
        />
        <div className="wrapper">
          <LeftBar
            categories={this.props.data.categories}
            api={this.props.data.api}
          />
          <div className="content-box">
            {
              (this.props.match.params.id != null) ? (
                <Composition 
                data={this.props.data.comps.find(elem => elem.id_comp==this.props.match.params.id)} 
                addToWanted={this.props.addToWanted}
                getProduct={this.props.getProduct}
                />
              ) : (
                  <div>{
                      this.props.data.comps.map(elem =>
                          <CompositionSmall
                            key={elem.id_comp}
                            data={elem} />
                      )
                    }</div>
                )
            }
          </div>
        </div>
        <Bottom />
      </div>
    )
  }
}

export default CompositionPage;