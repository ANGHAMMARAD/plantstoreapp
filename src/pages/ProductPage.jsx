import React from "react";
import {
  Redirect,
} from "react-router-dom";
import '../css/App.css'
import Menu from '../Menu'
import LeftBar from '../LeftBar'
import Product from '../Product'
import Bottom from '../Bottom'

class ProductPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
    }
  }

  componentDidMount(){
    let product_id = this.props.match.params.id;
    this.props.getProduct(product_id)
      .then(res => this.setState({data: res}))
  }

  render() {
    if (this.state.data == null) return null;
    return (
      <div className="root">
        <Menu api={this.props.data.api}
        />
        <div className="wrapper">
          <LeftBar
            categories={this.props.data.categories}
            api={this.props.data.api}
          />
          <div className="content-box">
            <Product
              data={this.state.data}
              lighting={this.props.data.lighting}
              watering={this.props.data.watering}
              soil={this.props.data.soil}
              sizes={this.props.data.sizes}
              wanted={this.props.data.wanted}
              cart={this.props.data.cart}
              addToWanted={this.props.addToWanted}
              addToCart={this.props.addToCart}
              getComposition={this.props.getComposition}
            />
          </div>
        </div>
        <Bottom />
      </div>
    )
  }
}

export default ProductPage;