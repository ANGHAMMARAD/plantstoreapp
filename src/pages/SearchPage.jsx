import React from "react";
import Menu from '../Menu'
import Bottom from '../Bottom'
import Search from '../Search'
import '../css/Search.css'

class SearchPage extends React.Component {

    render() {
        return (
            <div className="root">
                <Menu />
                <div className="wrapper">
                    <Search
                        data={this.props.data}
                        addToWanted={this.props.addToWanted}
                    />
                </div>
                <Bottom />
            </div>
        )
    }
}

export default SearchPage;