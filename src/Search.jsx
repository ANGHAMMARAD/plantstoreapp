import React from "react";
// import {
// } from "react-router-dom";

import Middle from './Middle'

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            current_items: null,
            loading: false,
            watering: [],
            light: [],
            soil: [],
        }
        this.handleSubmit = this.handleSubmit.bind(this)
        // this.handleChange = this.handleChange.bind(this)
    }

    componentDidMount() {
        let water = [...this.props.data.watering];
        let light = [...this.props.data.lighting];
        let soil = [...this.props.data.soil];
        let copy_w = water.map((elem) => {
            return {
                id: elem.id,
                description: elem.description,
                active: true
            }
        })
        let copy_l = light.map(elem => {
            return {
                id: elem.id,
                description: elem.description,
                active: true
            }
        })
        let copy_s = soil.map((elem) => {
            return {
                id: elem.id,
                description: elem.description,
                active: true
            }
        })
        this.setState({
            watering: copy_w,
            soil: copy_s,
            light: copy_l
        })
    }

    handleSubmit(query) {
        let filter={
            watering: [],
            light: [],
            soil: [],
        }
        this.state.watering.forEach(elem => {
            if(elem.active) filter.watering.push(elem.id)
        })
        this.state.soil.forEach(elem => {
            if(elem.active) filter.soil.push(elem.id)
        })
        this.state.light.forEach(elem => {
            if(elem.active) filter.light.push(elem.id)
        })
        fetch(process.env.REACT_APP_api + "/api/search/", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ query: query, filter: filter })
        })
            .then(res => res.json())
            .then((res) => {
                // console.log(JSON.stringify(res));
                console.log("ok")
                this.setState({ loading: false, current_items: res,  error: null })
            }).catch(err => {
                console.log(JSON.stringify(err));
                this.setState({ loading: false, error: err })
            })
    }

    changeLighting(id) {
        let copy = [...this.state.light]
        let index = copy.findIndex(elem => elem.id == id)
        if (index !== -1) {
            copy[index].active = !copy[index].active;
            this.setState({ light: copy })
        }
    }

    watering(id) {
        let copy = [...this.state.watering]
        let index = copy.findIndex(elem => elem.id == id)
        if (index !== -1) {
            copy[index].active = !copy[index].active;
            this.setState({ watering: copy })
        }
    }

    changeSoil(id) {
        let copy = [...this.state.soil]
        let index = copy.findIndex(elem => elem.id == id)
        if (index !== -1) {
            copy[index].active = !copy[index].active;
            this.setState({ soil: copy })
        }
    }

    watering_div(watering) {
        return (<div className="watering">{
            watering.map((elem) => {
                if (elem.active) return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.watering(elem.id)} type="checkbox" defaultChecked></input>
                        <p className="watering-active enviroment-element">{elem.description} потреба у поливі</p>
                    </div>
                )
                else return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.watering(elem.id)} type="checkbox" defaultChecked></input>
                        <p className="enviroment-element">{elem.description} потреба у поливі</p>
                    </div>
                )
            })}

        </div>);
    }

    lighting_div(light) {
        return (<div className="lighting">{
            light.map((elem) => {
                if (elem.active) return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.changeLighting(elem.id)} type="checkbox" defaultChecked></input>
                        <p key={elem.id} className="lighting-active enviroment-element">Освітлення: {elem.description}</p>
                    </div>
                )
                else return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.changeLighting(elem.id)} type="checkbox" defaultChecked></input>
                        <p key={elem.id} className="enviroment-element">Освітлення: {elem.description}</p>
                    </div>
                )
            })}
        </div>);
    }

    soil_div(soil) {
        return (<div className="soil">{
            soil.map((elem) => {
                if (elem.active) return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.changeSoil(elem.id)} type="checkbox" defaultChecked></input>
                        <p key={elem.id} onClick={() => this.changeSoil(elem.id)} className="soil-active enviroment-element">Ґрунти: {elem.description}</p>
                    </div>
                )
                else return (
                    <div key={elem.id} className="search_filter_row">
                        <input onClick={() => this.changeSoil(elem.id)} type="checkbox" defaultChecked></input>
                        <p key={elem.id} onClick={() => this.changeSoil(elem.id)} className="enviroment-element">Ґрунти: {elem.description}</p>
                    </div>
                )
            })}
        </div>);
    }

    render() {
        return (
            <div className="content-box content-box-center">
                <SearchForm
                    handleSubmit={this.handleSubmit}
                />
                <div className="container_grid">
                    {this.state.error && <><small style={{ color: 'red' }}>{JSON.stringify(this.state.error, null, 4)}</small><br /></>}<br />
                    {(this.state.loading) ? (<h2>Завантаження результату</h2>) : null}
                    <div className="content-search">
                        {(this.state.current_items == null) ? (<h2>Введіть пошуковий запит</h2>) :
                            (<Middle
                                items={this.state.current_items}
                                api={this.props.data.api}
                                wanted={this.props.data.wanted}
                                addToWanted={this.props.addToWanted}
                            />)}
                        <div className="enviroment">
                            {this.watering_div(this.state.watering)}
                            {this.lighting_div(this.state.light)}
                            {this.soil_div(this.state.soil)}
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default Search;

class SearchForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            query: ""
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event) {
        this.setState({ query: event.target.value })
        // console.log(event.target.value);
    }

    render() {
        return (
            <div className="searchForm">
                <input value={this.state.query} onChange={this.handleChange}
                    className="form-control" type="text" placeholder="Пошук" aria-label="Search"></input>
                <button onClick={() => {
                    // this.setState({query:""})
                    return this.props.handleSubmit(this.state.query)
                }} className="btn btn-outline-success btn-rounded btn-sm my-0">Пошук</button>
            </div>
        );
    }
}